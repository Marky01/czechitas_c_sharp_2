﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._1_Lekce_Opakovani
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Rozšiřte třídu rytíř a přidejte vlastnost Jmeno a StartovaciCislo.
            // 2. Nastavte každému novému rytíři jedinečné startovací číslo. Např. prvnímu 1, dalšímu 2, atd.
            // 3. Rozšiřte hlavní program, aby bylo možné pro rytíře zadat jméno i počet bodů.
            // 3a. Podobně, jako je funkce pro zadání čísla z konzole, vytvořte funkci, která načte libovolný text, text nesmí být prázdný nebo jen z mezer. Použijte funkci string.IsNullOrWhiteSpace(text), která umí toto zkontrolovat. Funkci pak použijte pro zadání jména.

            // 4. Vytvořte ve třídě Rytir metodu, která zobrazí informace o rytíři na konzoli: Rytíř č.1 - Bořivoj získal 12 bodů
            // 5. Rozšiřte výsledkovou listinu, aby se zobrazily informace o každém rytíři, využijte funkci pro zobrazení informací o rytíři. 

            Console.WriteLine("Program pro zobrazeni vysledku");
            Console.WriteLine("==============================");
            Console.WriteLine();

            int pocetRytiru = NactiCeleCisloZKonzole("Zadejte pocet rytiru: ");
            Console.WriteLine("Zadali jste " + pocetRytiru + " rytiru.");

            Rytir[] rytiri = new Rytir[pocetRytiru];

            for (int cisloRytire = 0; cisloRytire < pocetRytiru; cisloRytire++)
            {
                Console.WriteLine();
                Console.Write("Zadejte jmeno rytire c. {0}: ", cisloRytire + 1);
                string jmeno = NactiText();
                int pocetBodu = NactiCeleCisloZKonzole("Zadej body pro rytire c. " + (cisloRytire + 1) + ": ");
                Rytir rytir = new Rytir(jmeno, pocetBodu);
                rytiri[cisloRytire] = rytir;
            }

            Console.WriteLine();
            Console.WriteLine("Vysledkova listina");
            Console.WriteLine("------------------");
            for (int cisloRytire = 0; cisloRytire < rytiri.Length; cisloRytire++)
            {
                Rytir rytir = rytiri[cisloRytire];
                if (rytir != null)
                {
                    Console.WriteLine(rytir.VypisInformace());
                }
            }

            Console.ReadLine();
        }

        static int NactiCeleCisloZKonzole(string dotaz)
        {
            int cislo = 0;
            bool vysledekPrevodu = false;

            while (vysledekPrevodu == false)
            {
                Console.Write(dotaz);
                string pocetText = Console.ReadLine();
                vysledekPrevodu = int.TryParse(pocetText, out cislo);

                if (vysledekPrevodu == false)
                {
                    Console.WriteLine("Tohle neni cislo.");
                }
            }

            return cislo;
        }

        static string NactiText()
        {
            string s = "";
            bool isCorrect = true;
            while (isCorrect)
            {
                s = Console.ReadLine();
                isCorrect = string.IsNullOrWhiteSpace(s);
                if (isCorrect)
                {
                    Console.Write("Text je null nebo obsahuje jen mezery. Zadejte text znovu: ");
                }
            }
            return s;
        }
    }
}
