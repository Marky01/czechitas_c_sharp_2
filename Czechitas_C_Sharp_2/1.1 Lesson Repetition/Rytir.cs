﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._1_Lekce_Opakovani
{
    class Rytir
    {
        public int PocetBodu;
        public string Jmeno;
        public int StartovaciCislo;
        private static int PosledniStartovaciCislo = 0;

        public Rytir(string jmeno, int pocetBodu)
        {
            Jmeno = jmeno;
            PocetBodu = pocetBodu;
            PosledniStartovaciCislo++;
            StartovaciCislo = PosledniStartovaciCislo;
        }


        public string VypisInformace()
        {
            string info = "Rytir c. " + StartovaciCislo + " - " + Jmeno + " ziskal " + PocetBodu + " bodu.";
            return info;
        }
    }
}
