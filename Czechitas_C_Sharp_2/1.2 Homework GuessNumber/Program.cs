﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Napište program, který umožní hádat číslo. Zeptá se, jaké číslo si myslím. Podle toho, jaké číslo se zadá, napíše, jestli je číslo větší nebo menší a umožní hádat tak dlouho, dokud se uživatel nestrefí:
                Např.
                Hádej číslo: 10
                Číslo je menší, hádej znovu: 5
                Číslo je větší, hádej znovu: 7
                To je správně!
             */

            Console.WriteLine("Guess number between 1 and 100.");
            int userNumber = EnterNumber("Enter youre guess: ");

            Random random = new Random();
            int randomNumer = random.Next(0, 99);

            bool correct = false;

            while (!correct)
            {
                if (randomNumer > userNumber)
                {
                    userNumber = EnterNumber("Number is higher, guess again: ");
                }
                else if (randomNumer < userNumber)
                {
                    userNumber = EnterNumber("Number is lower, guess again: ");
                }
                else if (randomNumer == userNumber)
                {
                    correct = true;
                    Console.WriteLine();
                    ConsoleWriteLineCustomForegroundColor(ConsoleColor.Green, "This is correct!");
                }
            }
            Console.ReadLine();
        }

        /// <summary>
        /// This method ask user to enter number and handle user input if it's not a number.
        /// </summary>
        /// <param name="question">Custom question for number input.</param>
        /// <returns>Entered int number.</returns>
        public static int EnterNumber(string question)
        {
            bool numNotEntered = true;
            int number = 0;

            while (numNotEntered)
            {
                Console.WriteLine();
                Console.Write(question);
                numNotEntered = !int.TryParse(Console.ReadLine(), out number);
                if (numNotEntered)
                {
                    ConsoleWriteLineCustomForegroundColor(ConsoleColor.Red, "Input is not a number!");
                }
            }
            return number;
        }
        /// <summary>
        /// Writes console line in custom foreground color.
        /// </summary>
        /// <param name="consoleColor">Custom console color.</param>
        /// <param name="customText">Text to be colored.</param>
        public static void ConsoleWriteLineCustomForegroundColor(ConsoleColor consoleColor, string customText)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(customText);
            Console.ResetColor();
        }
    }
}
