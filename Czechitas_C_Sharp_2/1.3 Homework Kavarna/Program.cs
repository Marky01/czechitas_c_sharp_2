﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kavarna
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vytejte v nasem Coffee baru!");
            Console.WriteLine("============================");

            string[] nabidka = { "capuchino", "late", "cokolada", "limonada", "cheesecake", "vetrnik", "sandwich", "panini", "vino", "svarak" };
            Console.WriteLine("\nV nabídce máme: {0}", string.Join(", ", nabidka));
            Console.WriteLine("Co si dáte?");


            string name = "Marek";
            Console.WriteLine($"Ahoj, moje jméno je {name}");
            Console.WriteLine($"|{"Left",-7}|{"Right",7}|");


            bool ordering = true;
            string objednavka = "Vaše objednavka: ";
            while (ordering)
            {
                string item = Console.ReadLine().ToLower();

                if (nabidka.Contains(item))
                {
                    
                    objednavka = objednavka + item + ", ";
                    Console.WriteLine("Vyborne, a dal?");
                }
                else if (item == "hotovo")
                {
                    objednavka = objednavka.Substring(0, objednavka.Length - 2);
                    Console.WriteLine("\n" + objednavka);
                    ordering = false;
                }
                else 
                {
                    Console.WriteLine("Je nám líto, tuto možnost nenabízíme. Zkuste si vybrat něco jiného.");
                }
            }
        }
    }
}
