﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucisnik
{
    /// <summary>
    /// Archer with default value of 10 arrows who can shoot arrows.
    /// </summary>
    public class Archer
    {
        /// <summary>
        /// Current number of arrows.
        /// </summary>
        public int NumOfArrows { get; set; }

        /// <summary>
        /// Creates new instance of archer.
        /// </summary>
        public Archer()
        {
            NumOfArrows = 10;
        }

        /// <summary>
        /// Method shoot arrow if archers has at least one in NumOfArrows.
        /// </summary>
        public void Shoot()
        {
            if (NumOfArrows < 1)
            {
                Console.WriteLine("I don't have arrows.");
            }
            else
            {
                Console.WriteLine("I always hit right in the middle!");
                NumOfArrows--;
            }
        }
    }
}

