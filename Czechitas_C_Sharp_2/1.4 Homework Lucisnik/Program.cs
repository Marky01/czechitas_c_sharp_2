﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucisnik
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                - Vytvořte třídu Lucistnik, ktera bude mit vlastnost PocetSipu a metodu Vystrel. PocetSipu muze byt nastaven treba na 10.
                - Pokud bude mit dost sipu, napise Vystrel na konzoli text: Vzdy se strefim primo do prostred!
                - metoda Vystrel uvnitr s kazdym vystrelem snizi pocet sipu.
                - Pokud bude pocet 0, metoda Vystrel vypise "Nemam sipy".
                - Napiste program, ktery vytvori lucistnika a vystreli vsechny sipy
             */
            Archer jarda = new Archer();
            for (int i = jarda.NumOfArrows; i >= 0; i--)
            {
                jarda.Shoot();
            }
            Console.ReadKey();

        }
    }
}
