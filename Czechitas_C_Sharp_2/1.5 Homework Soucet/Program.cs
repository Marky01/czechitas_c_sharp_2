﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soucet
{
    class Program
    {
        static void Main(string[] args)
        {
            // Napište program, který se zeptá na dvě čísla a zobrazí jejich součet.
            Console.WriteLine("This program counts sum of two numbers.");

            int a = EnterNumber("first");
            int b = EnterNumber("second");
            int sum = a + b;

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Sum of {0} and {1} is {2}.", a, b, sum);
            Console.ResetColor();
            Console.ReadKey();

            // Q1: Bylo řečeno, že by funkce neměly inteagovat s konzolí. Jak by se to dělalo v tomto případě (enter text line v metodě EnterNumber())? Bylo by lepší udělat napsat pouze funkce vypisující text?
            // Q2: Jak moc detailní by měly být XML komentáře pro dokumentaci? Přesný popis toho co metoda dělá? Je dobré je psát i u metod, které nemají vlastní třídu nebo se píší hlavn2 pro třídy? 



        }
        /// <summary>
        /// This method ask user to enter number and handle user input if it's not a number.
        /// </summary>
        /// <param name="order">Order of entered number eg. first, second, third and so on.</param>
        /// <returns>Entered int number.</returns>
        public static int EnterNumber(string order)
        {
            bool numNotEntered = true;
            int number = 0;

            while (numNotEntered)
            {
                Console.WriteLine();
                Console.WriteLine("Enter {0} number:", order.ToLower());
                numNotEntered = !int.TryParse(Console.ReadLine(), out number);
                if (numNotEntered)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Input is not a number!");
                    Console.ResetColor();
                }
            }
            return number;
        }
    }
}
