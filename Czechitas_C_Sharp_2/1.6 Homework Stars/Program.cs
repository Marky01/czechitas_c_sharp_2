﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stars
{
    class Program
    {
        static void Main(string[] args)
        {
            //Napište program, který se zeptá na počet hvězdiček a potom je v cyklu zobrazí na konzoli.
            Console.WriteLine("Program to write out number stars according user input.");

            int input = EnterNumber("Enter number of stars: ");

            Console.WriteLine();
            for (int i = 0; i < input; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("*");
                Console.ResetColor();
            }
            Console.ReadLine();
        }

        /// <summary>
        /// This method ask user to enter number and handle user input if it's not a number.
        /// </summary>
        /// <param name="question">Question text about what is user should enter.</param>
        /// <returns>Entered int number.</returns>
        public static int EnterNumber(string question)
        {
            bool numNotEntered = true;
            int number = 0;

            while (numNotEntered)
            {
                Console.WriteLine();
                Console.Write(question);
                numNotEntered = !int.TryParse(Console.ReadLine(), out number);
                if (numNotEntered)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Input is not a number!");
                    Console.ResetColor();
                }
            }
            return number;
        }
    }
}
