﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson03
{
    public class Program
    {
        public static void Main()
        {
            // Kral chce usporadat velky turnaj
            // kterykoli rytir se muze zucastnit

            // List umí dělat dynamicky velké pole, musím vytvořit instanci listu se stejným typem

            List<Rytir> seznam = new List<Rytir>
            {
                new Rytir("Vojtech"),
                new Rytir("Prokop"),
                new Rytir("Spytihnev"),
                new Rytir("Stanislav")
            };



            //seznam.Add(new Rytir("Vaclav"));
            Rytir vaclav = new Rytir("Vaclav");
            seznam.Add(vaclav);
            //seznam.Add(vaclav);

            //seznam[2].Jmeno = "Spytihnev II.";
            //seznam[4].Jmeno = "Vaclav II.";

            Console.WriteLine(seznam.Count);
            Console.WriteLine(seznam.Capacity);


            foreach (Rytir rytir in seznam)
            {
                Console.WriteLine(rytir);
            }

            seznam.Remove(vaclav);
            Console.WriteLine();

            foreach (Rytir rytir in seznam)
            {
                if (rytir.Jmeno.StartsWith("P"))
                {
                    Console.Write("-->");
                }
                Console.WriteLine(rytir);
            }

            // Predikát - lambda funkce
            Rytir rytirNaP = seznam.Find(rytir => rytir.Jmeno.StartsWith("P"));


            Console.WriteLine();
            Console.WriteLine(rytirNaP);


            var rytiri = seznam.FindAll(rytir => rytir.Jmeno.StartsWith("S"));
            Console.WriteLine();
            foreach (var rytir in rytiri)
            {
                Console.WriteLine(rytir);
            }


            var zivotVicNezStro = seznam.FindAll(rytir => rytir.Zivot > 105);

            Console.WriteLine();
            Console.WriteLine(String.Join(Environment.NewLine, zivotVicNezStro));
            foreach (var item in zivotVicNezStro)
            {
                Console.WriteLine(item);

            }


            // 
            Dictionary<string, Rytir> soupiskaRytiru = new Dictionary<string, Rytir>()
            {
                {"Vaclav", new Rytir("Vaclav")},
                {"Rostislav", new Rytir("Rostislav")}
            };

            Rytir vaclav2 = new Rytir("Vaclav II.");
            soupiskaRytiru.Add("Vaclav II.", vaclav2);
            soupiskaRytiru.Add("Vaclav III.", vaclav2);


            var rytirVaclav = soupiskaRytiru["Vaclav"];

            Console.WriteLine(rytirVaclav);
            Console.WriteLine();

            foreach (var rytir in soupiskaRytiru.Values)
            {
                Console.WriteLine(rytir);
            }

            if (soupiskaRytiru.ContainsKey("Vaclav"))
            {
                Console.WriteLine("Našel jsem Václava");
            };

            soupiskaRytiru.Add("Prokop", new Rytir("Prokop"));



        }

        static bool ZacinaNaP(Rytir rytir)
        {
            return rytir.Jmeno.StartsWith("P");
        }

        static void Boj(Rytir r1, Rytir r2)
        {
            int score1 = r1.BojujNaTurnaji(r2);
            int score2 = r2.BojujNaTurnaji(r1);

            if (score1 == score2)
            {
                Console.WriteLine(String.Format("Remiza mezi {0} a {1}", r1.Jmeno, r2.Jmeno));
            }
            else if (score1 > score2)
            {
                Console.WriteLine(String.Format("{0} vyhral nad {1}", r1.Jmeno, r2.Jmeno));
            }
            else
            {
                Console.WriteLine(String.Format("{0} prohral s {1}", r1.Jmeno, r2.Jmeno));
            }
        }






        public static void Main2()
        {
            Drak drak = new Drak(32, 200);
            Console.WriteLine("Priletel k nam drak!");

            Rytir[] seznamRytiru = new Rytir[6]; // pole (array) instanci typu Rytir

            seznamRytiru[0] = new Rytir(9, 100); // Vojtech   POZOR, zacina se od indexu 0
            seznamRytiru[1] = new Rytir(12, 80); // Prokop
            seznamRytiru[2] = new Rytir(11, 110); // Borivoj
            seznamRytiru[3] = new Rytir(14, 100); // Vaclav
            seznamRytiru[4] = new Rytir(12, 130); // Stanislav
            seznamRytiru[5] = new Rytir(8, 100); // Spytihnev

            Console.WriteLine("Kral povolal pul tuctu rytiru, aby s drakem bojovali");

            int kolikatyRytir = 0;

            // cela patalie s drakem -- rytiri jdou postupne bojovat s drakem, dokud je drak nazivu (nebo dokud mame rytire na seznamu)
            while (drak.JeNazivu() && kolikatyRytir < seznamRytiru.Length && seznamRytiru[kolikatyRytir] != null)
            {
                Console.WriteLine();
                Console.WriteLine("Drakova aktualni kondice " + drak);

                Rytir rytirKteryPraveBojuje = seznamRytiru[kolikatyRytir];
                Console.WriteLine("Bojovat s nim bude " + rytirKteryPraveBojuje);

                // jeden souboj -- tak dlouho, dokud jsou drak i rytir nazivu
                while (rytirKteryPraveBojuje.JeNazivu() && drak.JeNazivu())
                {
                    // jedno kolo souboje -- rytir zautoci a pak drak zautoci
                    rytirKteryPraveBojuje.Zautoc(drak);
                    drak.Zautoc(rytirKteryPraveBojuje);

                    Console.WriteLine(drak);
                    Console.WriteLine(rytirKteryPraveBojuje);
                }

                kolikatyRytir++;
            }


            Console.WriteLine();
            Console.WriteLine("Vsichni rytiri: ");

            // vnitrek cyklu foreach se provede prave tolikrat, kolik je prvku v ridicim poli (seznamRytiru)
            // uvnitr cyklu v promenne aktualniRytir budu mit postupne vsechny prvky pole
            foreach (Rytir aktualniRytir in seznamRytiru)
            {
                if (aktualniRytir != null)
                {
                    Console.Write(aktualniRytir);
                    if (!aktualniRytir.JeNazivu())
                    {
                        Console.Write(" (cest jeho pamatce!)");
                    }
                    Console.WriteLine();
                }
            }


            Console.ReadLine();
        }
    }
}
