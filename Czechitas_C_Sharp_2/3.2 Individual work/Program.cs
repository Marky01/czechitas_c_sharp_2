﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_Individual_work
{
    class Program
    {
        static void Main(string[] args)
        {
            var cisla = new List<double>
        {
            0.046939913,
            0.295866297,
            0.852489925,
            0.84994766,
            0.96925183,
            0.946275497,
            0.688903175,
            0.553463564,
            0.59628254,
            0.645816929
        };

            // Vypis vsechna cisla na konzoli (nachystej si pro to funkci)
            Vypis(cisla);


            // Vypis na konzoli pocet cisel v seznamu
            VypisPocet(cisla);

            // Pridej cislo 0.5 do seznamu
            cisla.Add(0.5);

            // vypis prvni cislo ze seznamu, ktere je vetsi nez 0.8
            var prvniCislo = cisla.Find(cislo => cislo > 0.5);
            Console.WriteLine();
            Console.WriteLine("First number is : {0}", prvniCislo);

            // najdi nejvetsi cislo v seznamu, vypis, ktere to je, a odstran ho ze seznamu
            var maximum = cisla.Max();
            Console.Write("Maximum value is : {0}", maximum);
            cisla.Remove(maximum);
            Console.WriteLine();
            Console.WriteLine();

            cisla.Sort();
            var max = cisla[cisla.Count - 1];



            // vypis opet vsechna cisla a jejich pocet

            Vypis(cisla);
            VypisPocet(cisla);
        }

        private static void Vypis(List<double> list)
        {
            Console.WriteLine(String.Join(Environment.NewLine, list));
        }

        private static void VypisPocet(List<double> list)
        {
            Console.WriteLine(String.Format(" -- pocet cisel: {0}", list.Count));
        }
    }
}