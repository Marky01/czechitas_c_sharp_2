﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._3_Homework
{
    class Prihlasovani
    {
        List<Student> seznamStudentu;
        Dictionary<string, Predmet> katalogPredmetu;

        public Prihlasovani()
        {
            // 1. nejprve vytvor novy prazdny seznam studentu a uloz ho do promenne seznamStudentu
            seznamStudentu = new List<Student>();


            // naplnime studenty
            seznamStudentu.Add(new Student("Jan", "Novak"));
            seznamStudentu.Add(new Student("Petra", "Svobodova"));
            // 2. pridej nekolik dalsich studentu
            seznamStudentu.Add(new Student("Jakub", "Matýsek"));
            seznamStudentu.Add(new Student("Ondrej", "Novak"));
            seznamStudentu.Add(new Student("Jana", "Janikova"));


            // 3. vypis seznam studentu na konzoli
            Console.WriteLine("-- seznam studentu");
            foreach (var student in seznamStudentu)
            {
                Console.WriteLine(student);
            }

            // vytvorime katalog predmetu, vytvorime nejake predmety
            katalogPredmetu = new Dictionary<string, Predmet>
            {
                {"M001", new Predmet("M001", "Uvod do elektronove mikroskopie")},
                {"P001", new Predmet("P001", "Programovani C# 1")},
                {"P002", new Predmet("P002", "Programovani C# 2", new List<string> {"P001"})}
            };

            // pridame jeste dalsi predmety
            katalogPredmetu.Add("P020", new Predmet("P020", "Vyvoj mobilnich aplikaci", new List<string> { "X111" }));
            katalogPredmetu.Add("P021", new Predmet("P030", "Vzvoj aplikaci pro elektronove mikroskopy", new List<string> { "M001", "P002" }));
            // 4. pridej jeste dalsi predmety, aspon jeden bez prerekvizit a jeden s prerekvizitou
            katalogPredmetu.Add("P003", new Predmet("P003", "Objektově orientované programování", new List<string> { "P001", "P002" }));
            katalogPredmetu.Add("G001", new Predmet("G001", "Pocitacova grafika I."));
            katalogPredmetu.Add("G002", new Predmet("G002", "Pocitacova grafika II.", new List<string> { "G001" }));
            katalogPredmetu.Add("D001", new Predmet("D001", "Databazove systemy I."));
            katalogPredmetu.Add("D002", new Predmet("D002", "Databazove systemy II.", new List<string> { "D001" }));



            // 5. v metode ZkontrolujKatalog prover, jestli vsechny predmety katalogu maji v prerekvizitach existujici predmety
            // vypis chyby v nasledujicim formatu:
            //     Predmet <nazev> obsahuje neexistujici prerekvizitu: <kod>
            ZkontrolujKatalog(katalogPredmetu);


            // 6. najdi v seznamu studenta s prijmenim Novak, uloz ho do promenne studentNovak
            Student studentNovak = seznamStudentu.Find(student => student.Primeni.Equals("Novak"));


            // 7. naimplementuj metodu ZapisPredmet
            // ktera zapise danemu studentovi predmet (tedy ulozi kod predmetu do jeho seznamu ZapsaneAbsolvovanePredmety)
            // pokud predmet existuje v katalogu a pokud ma student uz zapsane vsechny prerekvizity
            // vypis uspesne zapsani nebo chybu v nasledujicim formatu:
            //     <jmeno>, <prijemni>: Uspesne zapsany predmet <nazev>
            //     <jmeno>, <prijemni>: Nebylo mozno zapsat predmet <nazev> (chybi prerekvizita)
            //     <jmeno>, <prijemni>: Nebylo mozno zapsat predmet <kod> (neexistujici predmet)
            ZapisPredmet(studentNovak, "P001");
            ZapisPredmet(studentNovak, "P002");
            ZapisPredmet(studentNovak, "P030");
            ZapisPredmet(studentNovak, "M001");
            ZapisPredmet(studentNovak, "P030");
            ZapisPredmet(studentNovak, "X222");
        }

        private void ZkontrolujKatalog(Dictionary<string, Predmet> katalog)
        {
            // Poznámka pro kouče: Bylo by možné tohle napsat nějak přes predikáty?
            Console.WriteLine("-- zkontroluj katalog");
            foreach (Predmet predmet in katalog.Values)
            {
                foreach (var prerekvizitaKod in predmet.PrerekvizityKod)
                {
                    if (!katalog.ContainsKey(prerekvizitaKod))
                    {
                        Console.WriteLine($"Predmet {predmet.Jmeno} obsahuje neexistující prerekvizitu: {prerekvizitaKod}.");
                    }
                }
            }
        }

        public void ZapisPredmet(Student student, string kodPredmetu)
        {
            Console.WriteLine("-- zapis predmet");
            string message = String.Empty;
            bool predmetExistuje = katalogPredmetu.ContainsKey(kodPredmetu);

            if (predmetExistuje)
            {
                bool chybiPrezekvizita = false;
                foreach (string prerekvizitaKod in katalogPredmetu[kodPredmetu].PrerekvizityKod)
                {
                    if (!chybiPrezekvizita)
                    {
                        chybiPrezekvizita = !student.ZapsaneAbsolvovanePredmety.Any(predmet => predmet.Kod.Contains(prerekvizitaKod));
                    }
                }
                if (!chybiPrezekvizita)
                {
                    student.ZapsaneAbsolvovanePredmety.Add(katalogPredmetu[kodPredmetu]);
                    message = $"{student}: Uspesne zapsany predmet {katalogPredmetu[kodPredmetu].Jmeno}";
                }
                else
                {
                    message = $"{student}: Nebylo mozno zapsat predmet {katalogPredmetu[kodPredmetu].Jmeno} (chybi prerekvizita)";
                }
            }
            else
            {
                message = $"{student} Nebylo mozno zapsat predmet {kodPredmetu} (neexistujici predmet)";
            }

            Console.WriteLine(message);
        }
    }
}
