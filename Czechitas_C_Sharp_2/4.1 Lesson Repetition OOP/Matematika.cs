﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._1_Lesson_Repetition_OOP
{
    static class Matematika
    {
		public static int Koeficient { get; set; }

		public static int VypocitejObsahObdelnika(int sirka, int vyska)
		{
			return sirka * vyska * Koeficient;
		}

		// nelze ve staticke tride
		//public void VytiskniMouInstanciMatematiky()
		//{
		//    Console.WriteLine($"{Koeficient}");
		//}
	}
}
