﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._2_Individual_work_OOP_and_Properties
{
    class Obdelnik
    {
        public int Sirka { get; private set; }
        public int Vyska { get; private set; }
        public int Obsah => Sirka * Vyska;
        public int Obvod => 2 * (Sirka + Vyska);


        public Obdelnik(int sirka, int vyska)
        {
            if (sirka == 0 || vyska == 0)
            {
                throw new ArgumentException(String.Format($"Sirka ani vyska obdelniku nesmi byt 0!"));
            }
            else
            {
                Sirka = sirka;
                Vyska = vyska;
            }
        }

        public void VypisInformace()
        {
            Console.WriteLine($"Vyska obdelniku je {Vyska}. Sirka obdelniku je {Sirka}. Obsah je {Obsah} a obvod je {Obvod}.");
        }

        public void ZvetsiObdelnik(int sirka, int vyska)
        {
            Sirka += sirka;
            Vyska += vyska;
        }
    }
}
