﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._2_Individual_work_OOP_and_Properties
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Vytvorte tridu Obdelnik, ktera bude mit vlastnosti Sirka a Vyska jako properties.
               Nastavte properties tak, aby je nebylo mozne zmenit po vytvoreni instance.
               Obdelniku vytvorte funkci Vypis informace, ktery vypise vysku a sirku.
               Vytvorte property Obsah, ktera umozni ziskat obsah obdelniku.
               Vytvorte property Obvod, ktera umozni ziskat obvod obdelniku.
               Vytvorte funkci Zvetsi, ktera zvetsi obdelnik o sirku a vysku.
               Zajistete, aby nebylo mozne vytvorit obdelnik o obsahu 0.
               Napiste program, ktery vytvori obdelnik, vypise jeho velikosti, obsah a obvod.
            */

            Obdelnik obdelnik = new Obdelnik(2, 3);
            obdelnik.VypisInformace();
            obdelnik.ZvetsiObdelnik(2, 3);
            obdelnik.VypisInformace();
            
        }
    }
}
