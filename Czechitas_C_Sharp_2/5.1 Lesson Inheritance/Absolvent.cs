﻿namespace _5._1_Lesson_Inheritance
{
    public class Absolvent : Student
    {
        public Absolvent(string jmeno, string prijmeni, int rokUkonceni) : base (jmeno, prijmeni)
        {
            RokUkonceni = rokUkonceni;
        }
        public int RokUkonceni { get; set; }
    }
}
