﻿using System;

namespace _5._1_Lesson_Inheritance
{
    public abstract class Osoba
    {
        protected Osoba(string jmeno, string prijmeni)
        {
            Jmeno = jmeno;
            Prijmeni = prijmeni;
        }
        public string Jmeno { get; protected set; }
        public string Prijmeni { get; protected set; }

        public virtual void PrihlasSeDoSkolnihoSystemu()
        {
            Console.WriteLine($"{this}: Prihlasen do systemu.");
        }

        public abstract void NaobedvejSe();

        public override string ToString()
        {
            return String.Format($"{Jmeno} {Prijmeni}");
        }
    }
}
