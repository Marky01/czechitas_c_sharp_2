﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._1_Lesson_Inheritance
{

    public class Student : Osoba
    {

        public Student(string jmeno, string prijmeni) : base(jmeno, prijmeni)
        {
            ZapsaneAbsolvovanePredmety = new List<Predmet>();
        }

        public List<Predmet> ZapsaneAbsolvovanePredmety { get; private set; }

        public override void NaobedvejSe()
        {
            Console.WriteLine($"{this}: Objedvam v menze");
        }
    }
}
