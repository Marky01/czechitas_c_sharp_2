﻿using System;

namespace _5._1_Lesson_Inheritance
{
    public class Vyucujici : Osoba
    {
        public Vyucujici(string jmeno, string prijmeni) : base(jmeno, prijmeni)
        {
        }

        public Vyucujici(string jmeno, string prijmeni, string email) : this(jmeno, prijmeni)
        {
            Email = email;
        }

        public string Email { get; private set; }

        public override void PrihlasSeDoSkolnihoSystemu()
        {
            Console.WriteLine($"{this}: Ano jsem zamestnancem univerzity");
            base.PrihlasSeDoSkolnihoSystemu();           
        }

        public bool MaRadSvestky()
        {
            return Prijmeni.Length > 5;
        }

        public override void NaobedvejSe()
        {
            Console.WriteLine($"{this}: Objedvam v luxusni restaraci");
        }
    }
}
