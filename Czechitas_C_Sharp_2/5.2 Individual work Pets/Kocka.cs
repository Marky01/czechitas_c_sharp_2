﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._2_Individual_work_Pets
{
    class Kocka : DomaciMazlicek
    {
        public Kocka(string jmeno)
        {
            Jmeno = jmeno;
        }
        public void UlovitMys()
        {
            Console.WriteLine($"{Jmeno}: mys ulovena");
        }
    }
}
