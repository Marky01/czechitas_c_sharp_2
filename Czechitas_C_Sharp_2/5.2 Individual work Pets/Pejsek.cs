﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._2_Individual_work_Pets
{
    class Pejsek : DomaciMazlicek
    {
        public Pejsek(string jmeno)
        {
            Jmeno = jmeno;
        }

        public void Aportuj()
        {
            Console.WriteLine($"{Jmeno}: Aportoval klacik");
        }
    }
}
