﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._2_Individual_work_Pets
{
    class Program
    {
        
        static void Main(string[] args)
        {
            DomaciMazlicek tapka = new Pejsek("Tapka");
            tapka.NakrmSe();
            Pejsek alik = new Pejsek("Alik");
            alik.Aportuj();

            Kocka kocka = new Kocka("Micka");
            kocka.UlovitMys();
            kocka.NakrmSe();
        }


    }
}
