﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._1_Lesson_Interface
{
    class Program
    {

        /* HLAVNÍ INFO O INTERFACE JE V INDIVIDUAL WORK PROJEKTU */
        static void Main(string[] args)
        {

            Kocka kocka = new Kocka();
            kocka.ZobrazInformace();

            Zvire zvire = new Pes();
            zvire.ZobrazInformace();


            Console.ReadLine();
        }
    }
}
