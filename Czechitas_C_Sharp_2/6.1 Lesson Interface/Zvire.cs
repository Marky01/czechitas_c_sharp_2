﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._1_Lesson_Interface
{
    abstract class Zvire
    {
        public string Druh { get; set; }

        public abstract void ZobrazInformace();
    }

    class Kocka : Zvire
    {
        public override void ZobrazInformace()
        {
            Console.WriteLine("Jsem Kocka");
        }
    }

    class Pes : Zvire
    {
        public override void ZobrazInformace()
        {
            Console.WriteLine("Jsem Pes");
        }
    }
}
