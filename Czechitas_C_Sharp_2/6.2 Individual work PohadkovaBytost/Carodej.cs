﻿using System;

namespace _6._2_Individual_work_PohadkovaBytost
{
    class Carodej : KouzelnaPohadkovaBytost, ITancujiciBytost
    {
        public override void NapisJakTravisVolnyCas()
        {
            Console.WriteLine("Varim pernicek");
        }

        public void PrestanTancovat()
        {
            Console.WriteLine("Jako carodej uz nebudu tancovat.");
        }

        public void ZacniTancovat()
        {
            Console.WriteLine("Jako carodej jdu tancovat.");
        }
    }
}
