﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._2_Individual_work_PohadkovaBytost
{
    interface ITancujiciBytost
    {
        //List<string> Tance { get; set; }
        void ZacniTancovat();
        void PrestanTancovat();
    }
}
