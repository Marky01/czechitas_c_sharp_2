﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._2_Individual_work_PohadkovaBytost
{
    abstract class KouzelnaPohadkovaBytost : PohadkovaBytost
    {
        public void Zmiz()
        {
            Console.WriteLine( "Mizím....");
        }

        public void ObjevSe()
        {
            Console.WriteLine( "Objevuji se....");
        }
    }
}
