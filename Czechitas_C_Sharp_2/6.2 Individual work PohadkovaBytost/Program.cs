﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._2_Individual_work_PohadkovaBytost
{

    
    partial class Program
    {

        static void Main(string[] args)
        {
            PohadkovaBytost princezna = new Princezna();
            princezna.NapisJakTravisVolnyCas();
            
            TancujiciPohadkovaBytost princ = new Princ();
            princ.NapisJakTravisVolnyCas();
            princ.ZacniTancovat();
            Princ nyniJePrinc = princ as Princ; // Pokud bych zkusil Princeznu, vratí null
            nyniJePrinc = (Princ)princ;
            nyniJePrinc.VytasMec();
            (princ as Princ).VytasMec();

            PohadkovaBytost carodej = new Carodej();
            carodej.NapisJakTravisVolnyCas();

            princ.PrestanTancovat();

            ITancujiciBytost tancujiciBytost = carodej as ITancujiciBytost;
            tancujiciBytost.ZacniTancovat();
            carodej.NapisJakTravisVolnyCas();

            Console.WriteLine("------------------------------");
            List<PohadkovaBytost> pohadkoveBytosti = new List<PohadkovaBytost>() { princezna, princ, carodej };
            foreach (ITancujiciBytost tanecnik in pohadkoveBytosti)
            {
                tanecnik.ZacniTancovat();
                tanecnik.PrestanTancovat();
            }

            Console.WriteLine("------------------------------");
            foreach (KouzelnaPohadkovaBytost kouzelnaBytost in pohadkoveBytosti.OfType<KouzelnaPohadkovaBytost>()) 
            {
                kouzelnaBytost.Zmiz();
                
            }
        }
    }
}
