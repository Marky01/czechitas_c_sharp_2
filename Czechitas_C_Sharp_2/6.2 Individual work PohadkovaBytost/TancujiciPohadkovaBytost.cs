﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._2_Individual_work_PohadkovaBytost
{
    class TancujiciPohadkovaBytost : PohadkovaBytost, ITancujiciBytost
    {

        public override void NapisJakTravisVolnyCas()
        {
            Console.WriteLine("Tancuju");
        }

        public void PrestanTancovat() // cokoli co pochazi z interface, tak musi byt public
        {
            Console.WriteLine("Jdu si sednout");
        }

        public void ZacniTancovat()
        {
            Console.WriteLine("Jdu tancovat");
        }
    }
}
