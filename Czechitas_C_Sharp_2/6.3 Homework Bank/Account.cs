﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._3_Homework_Bank
{
    public class Account : IAccount
    {
        public int Balance { get; set; }
        public string Owner { get; private set; }

        public Account(string owner, int openingBalance)
        {
            Owner = owner;
            Balance = openingBalance;
        }

        public override string ToString()
        {
            return String.Format($"{Owner}: {Balance}");
        }
    }
}
