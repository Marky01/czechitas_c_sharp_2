﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._3_Homework_Bank
{
    class Bank 
    {
        public Dictionary<string, Account> accounts { get; private set; }

        public Bank()
        {
            accounts = new Dictionary<string, Account>();
        }

        public void CreateAccount(string owner, int openingBalance)
        {
            Account account = new Account(owner, openingBalance);
            accounts.Add(owner, account);
        }

        public IAccount FindAccount(string owner)
        {
            IAccount iAccount = accounts[owner];
            return iAccount;
        }

        public void SaveAmount(string owner, int amount)
        {
            IAccount iAccount = FindAccount(owner);
            Account account = iAccount as Account;
            account.Balance += amount;
        }
    }
}
