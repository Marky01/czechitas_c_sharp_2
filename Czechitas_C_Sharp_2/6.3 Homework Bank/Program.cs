﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6._3_Homework_Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank czechBank = new Bank();
            czechBank.CreateAccount("Josef Horak", 0);
            czechBank.CreateAccount("Martin Valek", 10000);
            czechBank.CreateAccount("Veronika Sodkova", 50);

            Console.WriteLine("--- initial state");
            foreach (Account savingsAccount in czechBank.accounts.Values)
            {
                Console.WriteLine(savingsAccount);
            }

            
            czechBank.SaveAmount("Josef Horak", 50000);
            
            Console.WriteLine("--- after saving amount");
            foreach (Account savingsAccount in czechBank.accounts.Values)
            {
                Console.WriteLine(savingsAccount);
            }

            IAccount iSavingsAccount = czechBank.FindAccount("Veronika Sodkova");
            //iSavingsAccount.Balance = 100;
        }
    }
}
